import React, { useState } from 'react';
import category from '../Images/category_icon.png';
import cube from '../Images/cube.png';
import lachlan from '../Images/lachlan.jpg';
import list from '../Images/list.png';
import messenger from '../Images/messenger.png';
import nonamesontheway from '../Images/nonamesontheway.jpg';
import plus from '../Images/plus.png';
import raamin from '../Images/raamin.jpg';
import security from '../Images/security.png';
import success from '../Images/success.png';
import users from '../Images/users.png';


export default function 
() {
  return (
    <div>
        <div class="flex flex-col items-center w-32 h-full overflow-hidden text-gray-700 bg-gray-100 rounded">
            <div class="flex flex-col items-center mt-3 border-gray-300">
                <a class="flex items-center justify-center mt-3 w-7 h-7 " href="#">
                    <img src={category} alt="" className='w-full h-full'/>
                </a>
                <a class="flex items-center justify-center w-7 h-7 mt-14 rounded hover:bg-gray-300" href="#">
                    <img src={cube} alt="" className='w-full h-full'/>
                </a>
                <a class="flex items-center justify-center w-7 h-7 mt-5 rounded hover:bg-gray-300" href="#">
                <img src={list} alt="" className='w-full h-full'/>
                </a>
                <a class="flex items-center justify-center w-7 h-7 mt-5 bg-gray-300 rounded" href="#">
                <img src={messenger} alt="" className='w-full h-full'/>
                </a>
                <a class="flex items-center justify-center w-7 h-7 mt-5 rounded hover:bg-gray-300" href="#">
                <img src={list} alt="" className='w-full h-full'/>
                </a>
                <a class="flex items-center justify-center w-7 h-7 mt-14 rounded hover:bg-gray-300" href="#">
                <img src={success} alt="" className='w-full h-full'/>
                </a>
                <a class="flex items-center justify-center w-7 h-7 mt-5 rounded hover:bg-gray-300" href="#">
                <img src={security} alt="" className='w-full h-full'/>
                </a>
                <a class="flex items-center justify-center w-7 h-7 mt-5 rounded hover:bg-gray-300" href="#">
                <img src={users} alt="" className='w-full h-full'/>
                </a>
                <a class="flex items-center justify-center mt-14 w-7 h-7 bg-gray-200 hover:bg-gray-300" href="#">
                <img src={lachlan} alt="" className='w-full h-full rounded-full'/>
                </a>
                <a class="flex items-center justify-center w-7 h-7 mt-5 hover:bg-gray-300" href="#">
                <img src={raamin} alt="" className='w-full h-full rounded-full'/>
                </a>
                <a class="flex items-center justify-center w-7 h-7 mt-5 hover:bg-gray-300" href="#">
                <img src={nonamesontheway} alt="" className='w-full h-full rounded-full'/>
                </a>
                <a class="flex items-center justify-center mt-5 hover:bg-gray-300" href="#">
                <img src={plus} alt="" className='w-7'/>
                </a>
            </div>
        </div>
    </div>
  )
}
