import React from 'react'
import { useState } from 'react';

export default function CardComponent({trip,setTrip}) {
  const [newTrip , setNewTrip] = useState({});

  // handle read detail
  const ReadDetail = (id) => {
    setNewTrip(id)
  }
  // handle change button
  const changButton = (id)=>{
      const buttonStatus = trip.map((data)=>{
        if(data.id ==id){
            if(data.status==="beach") return {...data,status:"mountain"}
            else if(data.status==="mountain") return {...data,status:"forest"}
            else if(data.status==="forest") return {...data,status:"beach"}
          
        }
        return data
      })
      setTrip(buttonStatus)
  }
  // handle addNew
  const addNewInputForm = (data) => {
    const {name, value} = data.target;
    setNewTrip({...newTrip, [name]:value})
    console.log(newTrip)
  }

  // handle submit button add new
  const submitNew = (data) => {
    setTrip([...trip, {...newTrip, id: trip.length + 1}]);
  };


  return (
    <div className='flex flex-col px-5'>
      <div className='flex flex-row justify-between mt-16'>
        <span className='text-black font-bold text-3xl'>Good Evening Team!</span>
        <div>
        {/* button add new trip */}
        <label htmlFor="my-modal-2" className="btn bg-slate-600">
        Add new trip
        </label>
        </div>
      </div>
      {/* Card */}
      <div className='grid grid-cols-3 mt-10 gap-2'>
      {trip.map((data) => (
        <div className="card text-neutral-content w-80 bg-cyan-900">
          <div className="card-body">
            <h2 className="card-title uppercase">{data.title}</h2>
            <p className='text-white line-clamp-3'>{data.description}</p>
            <p className='mt-2'>People Going</p>
            <p className='font-bold text-2xl'>{data.peopleGoing}</p>
            <div className="card-actions justify-between">
              {/* button change status */}
              <button type="button" onClick={()=>changButton(data.id)} className={`py-2.5 mb-2 rounded-lg w-32 ${data.status=='beach'?'bg-blue-500':data.status=='mountain'?'bg-slate-400':data.status=='forest'?'bg-green-400':'bg-red-600'}`}>
                {data.status != null ? data.status: "null"}
              </button>
              {/* button read detail */}
              <label 
              onClick={() =>ReadDetail(data)}
              htmlFor="my-modal-3" className="btn bg-gray-600">Read Detail</label>
            </div>
          </div>
        </div>
      ))}

      </div>
      {/* modal of read detail*/}
      <input type="checkbox" id="my-modal-3" className="modal-toggle"  />
      <div className="modal">
        <div className="modal-box relative">
          <label htmlFor="my-modal-3" className="btn btn-sm btn-circle absolute right-2 top-2">✕</label>
          <h3 className="text-lg font-bold uppercase">{newTrip.title}</h3>
          <p className="py-4">{newTrip.description}</p>
          <p className='mt-5 text-lg'>Around {newTrip.peopleGoing} People going there</p>
        </div>
      </div>

      {/* modal add new trip */}
      <input type="checkbox" id="my-modal-2" className="modal-toggle"/>
      <div className="modal">
        <div className="modal-box relative">
          <label htmlFor="my-modal-2" className="btn btn-sm btn-circle absolute right-2 top-2">✕</label>
            {/* Title */}
            <div className="form-control w-full py-2">
              <span className="label-text text-xl">Title</span>
              <input name='title' onChange={addNewInputForm} type="text" placeholder="Sihaknou village " className="input input-bordered input-primary w-full" />
            </div>
            {/* Description */}
            <div className="form-control w-full py-2">
              <span className="label-text">Description</span>
              <input name='description' onChange={addNewInputForm} type="text" placeholder="Happy place with beautiful beach" className="input input-bordered input-primary w-full"/>
            </div>
            {/* People going */}
            <div className="form-control w-full py-2">
              <span className="label-text">People going</span>
              <input name='peopleGoing' onChange={addNewInputForm} type="text" placeholder="3200" className="input input-bordered input-primary w-full"/>
            </div>
            {/* Type of Adventure */}
            <div className="form-control w-full py-2">
              <span className="status">Type of Adventure</span>
              <select name='status' onChange={addNewInputForm} className="select select-primary w-full">
                <option disabled selected>.... Choose any option ....</option>
                <option>beach</option>
                <option>mountain</option>
                <option>forest</option>
              </select>
            </div>
            {/* button Submit */}
            <button onClick={submitNew} className="btn btn-active btn-ghost">Button</button>
          </div>
      </div>
    </div>
    
  )
}
