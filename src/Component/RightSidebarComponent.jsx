import React from 'react';
import notifications from '../Images/notification.png';
import comment from '../Images/comment.png';
import christina from '../Images/christina.jpg';
import raamin from '../Images/raamin.jpg';
import nonamesontheway from '../Images/nonamesontheway.jpg';
import lachlan from '../Images/lachlan.jpg';


export default function 
() {
  return (
    <div>
      <div className='flex flex-col h-screen w-96 bg-camping px-5 relative'>
        {/* info of static notification */}
        <div className='flex items-center absolute gap-6 right-14 mt-3'>
          <img src={notifications} alt="" className='w-8 h-8'/>
          <img src={comment} alt="" className='w-8 h-8'/>
          <img src={lachlan} alt="" className='w-8 h-8 rounded-full'/>  
        </div>
        {/* button My Amazing Trip */}   
        <button className='bg-orange-200 btn btn-accent absolute right-10 top-20'>My Amazing Trip</button>
        {/* text */}
        <div className='absolute font-bold top-36 text-3xl text-white'>
          <p>I like laying down one the sand and looking at the moon</p>
        </div>
        {/* List people */}
        <div className='absolute top-80'>
          <p className='text-white'>27 people going to the trip</p>
          <div className='flex items-center gap-5'>
            <img src={lachlan} alt="" className='w-10 h-10 rounded-full mt-5'/>
            <img src={raamin} alt="" className='w-10 h-10 rounded-full mt-5 border-2 border-l-white'/>
            <img src={nonamesontheway} alt="" className='w-10 h-10 rounded-full mt-5 border-2 border-orange-600'/>
            <img src={christina} alt="" className='w-10 h-10 rounded-full mt-5 border-2 border-l-white'/>
            <button className='bg-orange-100 rounded-full mt-5 w-10 h-10 border-2 border-orange-600'>23+</button>
          </div>
        </div>
      </div>
    </div>
  )
}

