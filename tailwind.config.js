/** @type {import('tailwindcss').Config} */
module.exports = {
    purge: ["./src/**/*.{js,jsx,ts,tsx}"],
    theme: {
        extend: {
            backgroundImage: theme => ({
                "camping": "url('../public/camping.jpg')",
            }),
        },
    },
    extend: {   
    },
    plugins: [
    require("daisyui"),
    require('@tailwindcss/line-clamp'),
    ],
   // daisyUI config (optional)
    daisyui: {
        styled: true,
        themes: false,
        base: true,
        utils: true,
        logs: true,
        rtl: false,
        prefix: "",
        darkTheme: "dark",
    },
};